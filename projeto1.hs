module Projeto1 where

import Data.List

type Coordenadas = (Int, Int)



type Mapa = [[Peca]]

data Direcao = Este | Oeste
data Jogador = Jogador Coordenadas Direcao Bool
data Jogo = Jogo Mapa Jogador




data Peca = Bloco | Porta | Caixa | Vazio 
  deriving (Show,Eq) 

--validaPotencialMapa :: [(Peca, Coordenadas)] -> Bool
--validaPotencialMapa [] = False
--validaPotencialMapa ((h , (x,y)):t) | 
{-
where
mapa = ((h , (x,y)):t)
(unPeca,unCoor) = unzip ((h,(x,y)):t)
(unX,unY)= unzip unCoor
-}

--1)



posDif :: [(Peca, Coordenadas)] -> Bool
posDif [] = True
posDif ((h , (x,y)):t) | (h , (x,y)) `notElem` t = True && posDif t 
                       | otherwise = posDif t  


--2)


extPorta :: [(Peca, Coordenadas)] -> Bool
extPorta [] = False
extPorta ((h , (x,y)):t) = contaPortas ((h , (x,y)):t) == 1 


contaPortas :: [(Peca, Coordenadas)] -> Int
contaPortas [] = 0
contaPortas ((h ,_):t) | h == Porta = 1+contaPortas t 
                       | otherwise = contaPortas t




--3)

confirmaCaixa :: [(Peca, Coordenadas)] ->  [(Peca, Coordenadas)] -> Bool
confirmaCaixa [] mapa = True
confirmaCaixa ((h ,(x,y)):t) mapa | h == Caixa && confirmaPecaBaixo (x,y+1) mapa = True
                                  | h /= Caixa = confirmaCaixa t mapa
                                  | otherwise = False


confirmaPecaBaixo :: Coordenadas -> [(Peca, Coordenadas)] ->Bool
confirmaPecaBaixo (xc,yc) [] =  False
confirmaPecaBaixo (xc,yc) ((h,(x,y)):t) | (xc,yc) == (x,y) && (h == Caixa || h == Bloco) = True
                                        | (xc,yc) == (x,y) && (h /= Caixa && h /= Bloco) = False
                                        | otherwise = confirmaPecaBaixo (xc,yc) t  

--4)


existVazio :: [(Peca, Coordenadas)] -> Bool 
existVazio [] = True
existVazio mapa = contaVazio mapa > 0 


contaVazio :: [(Peca, Coordenadas)] -> Int
contaVazio [] = 0
contaVazio ((h ,_):t) | h == Vazio = 1+contaVazio t 
                      | otherwise = contaVazio t


-- Poderia acabar a funçao em 1 em vez de percorrer a função toda e ver todos os vazios

maiorOrdenada :: [Int] -> Int
maiorOrdenada l = maximum l 

maiorAbcissa :: [Int] -> Int
maiorAbcissa l = maximum l



--5)

ponto5daTarefa1 :: [(Peca, Coordenadas)] -> Bool
ponto5daTarefa1 lo = all (\(x,l) -> Bloco == (fst $ last l)) l
    where l = ordenaColunas lo

ordenaColunas :: [(Peca, Coordenadas)] -> [(Int, [(Peca, Int)])] -- Peças por ordem, mas colunas não
ordenaColunas [] = []
ordenaColunas l'@((p,(x,y)):t) = (x,l) : ordenaColunas t
    where
        l = sortOn snd $ aux x l'
        -- aux :: Int -> [(Peca, Coordenada)] -> [(Peca,Int)] -- Int é a linha
        aux _ [] = []
        aux i ((p,(x,y)):t)
            | x == i = (p,y) : aux i t
            | otherwise = aux i t



validaPotencialMapa :: [(Peca, Coordenadas)] -> Bool

validaPotencialMapa ((h , (x,y)):t) = (not (posDif mapa))
                                    && (not $ extPorta mapa) 
                                    && (not $ confirmaCaixa ((h , (x,y)):t) mapa) 
                                    && (((maiorAbcissa unX)+1) * ((maiorOrdenada unY)+1)) == length mapa && not(existVazio mapa) 

                                     where
                                     mapa = ((h , (x,y)):t)
                                     (unPeca,unCoor) = unzip ((h,(x,y)):t)
                                     (unX,unY)= unzip unCoor


--sortOn (fst . snd) $ sortOn (snd . snd)

{-
ZE

(unPeca,unCoor)=unzip ((h,(x,y)):t) 
(unX,unY)=unzip unCoor

maiorOrdenada :: [Int] -> Int
maiorOrdenada (y:t) = maiorCoor y t 

maiorCoor:: Int -> [Int] -> Int 
maiorCoor x [] = x 
maiorCoor x (h:t) | x> h = maiorCoor x t
                  | otherwise = maiorCoor h t  
-}
--searchBloco :: [(Peca, Coordenadas)] -> (Peca,Coordenadas)
--searchBloco []  = (Bloco,(0,0))
--searchBloco ((h,(0,y)):t) | h == Bloco &&  y == maiorOrdenada = (h,(0,y):t)
  --                        | otherwise= searchBloco ((h,(0,y-1)):t)





{-

LARA

--forma uma lista de blocos e as suas coordenadas
selecionaBlocos :: [(Peca, Coordenadas)] -> [(Peca, Coordenadas)]
selecionaBlocos []  = []
selecionaBlocos ((h,(x,y)):t) | h == Bloco = ((h,(x,y)) : selecionaBlocos t) 
                              | otherwise = selecionaBlocos t

--recebe uma lista de blocos e as respetivas coordenadas e escolhe a que tem menor abcissa e maior ordenada
escolheCoordenada :: [Coordenadas] -> Coordenadas  
escolheCoordenada 

--confirma se um
confirmaBlocos ::


-}








